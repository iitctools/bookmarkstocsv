// ==UserScript==
// @name           IITC plugin: Bookmarks to *.csv exporter
// @namespace      http://tampermonkey.net/
// @version        0.0.001a
// @description    Export Bookmarks to csv.
// @author         ATisch
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};

    //PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
    //(leaving them in place might break the 'About IITC' page or break update checks)
    plugin_info.buildName = 'iitc';
    plugin_info.dateTimeVersion = '20170920.2322';
    plugin_info.pluginId = 'bookmarksToCsvExporter';
    //END PLUGIN AUTHORS NOTE


    window.plugin.bookmarksToCsvExporter = function(){};

    window.plugin.bookmarksToCsvExporter.loadExternals = function() {
        try { console.log('Loading bookmarksToCsvExporter JS now...'); } catch(e) {}
        //add options menu
        $('#toolbox').append('<a onclick="window.plugin.bookmarksToCsvExporter.Export();return false;" accesskey="b" title="[b]">Export BookmarksToCsv</a>');
        try { console.log('bookmarksToCsvExporter JS loaded'); } catch(e) {}
    };

    window.plugin.bookmarksToCsvExporter.Export = function() {
        if(window.plugin.bookmarks !== undefined){
            var portalData = "";
            var objs = window.plugin.bookmarks.bkmrksObj['portals'];
            var exportStr = "Portal group;Portal name;Intel link\n";
            for (var idFolders in objs){
                var folders = objs[idFolders];
                var folderName = folders['label'];
                var fold = folders['bkmrk'];
                for (var idBkmrk in fold){
                    var bkmrk = fold[idBkmrk];
                    var label = bkmrk['label']; //Portal name
                    var latlng = bkmrk['latlng']; //coordinates
                    var link = "https://www.ingress.com/intel?ll="+latlng+"&z=15&pll="+latlng;
                    exportStr = exportStr + folderName + ";" + label +";" + link + "\n";
                }
            }
            alert(exportStr);

        } else {
            alert('This plugin needs IITC plugin: Bookmarks for maps and portals');
        }
    };

    var setup = window.plugin.bookmarksToCsvExporter.loadExternals;

// PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end

// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);
